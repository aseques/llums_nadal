#!/usr/bin/env python3
 
import RPi.GPIO as GPIO, time
#install py-getch from pip3
import getch
import sys
 
DEBUG = 1
ultim = ''
 
GPIO.setmode(GPIO.BCM)
RED_LED = 18
BLUE_LED1 = 22
BLUE_LED2 = 23
BLUE_LED3 = 17
GPIO.setup(RED_LED, GPIO.OUT)
GPIO.setup(BLUE_LED1, GPIO.OUT)
GPIO.setup(BLUE_LED2, GPIO.OUT)
GPIO.setup(BLUE_LED3, GPIO.OUT)

def giratots():
    giraun(RED_LED)
    giraun(BLUE_LED1)
    giraun(BLUE_LED2)
    giraun(BLUE_LED3)

def giraun(numled):
    if GPIO.input(numled):
        GPIO.output(numled, GPIO.LOW)
    else:
        GPIO.output(numled, GPIO.HIGH)

def offon(numled):
    #si esta engegat apago
    if GPIO.input(numled):
        GPIO.output(numled, GPIO.LOW)
    #engego
    GPIO.output(numled, GPIO.HIGH)

try:
    while True:
        char = getch.getch()
        if char == 'a' and ultim != 'a':
            print("A d'Ariadna ...")
            offon(BLUE_LED1)
            offon(BLUE_LED2)
            offon(BLUE_LED3)
        elif char == 'q':
            GPIO.cleanup()
            sys.exit()
        else:
            giratots()
        ultim = char
except KeyboardInterrupt:
    GPIO.cleanup()
