#!/usr/bin/env python3
import datetime
import os.path
from subprocess import call
from subprocess import check_output

def time_in_range(start, end, x):
    """Return true if x is in the range [start, end]"""
    if start <= end:
        return start <= x <= end
    else:
        return start <= x or x <= end

def engega(codi):
    #comprovem estat si ja estava engegat
    estat = check_output(["/usr/local/bin/heyu","onstate",codi]).rstrip()
    if estat.decode('ascii') == "0":
        print(codi+" encara no estava engegat, engeguem")
        call(["/usr/local/bin/heyu","on",codi])


def apaga(codi):
    #comprovem estat anterior
    estat = check_output(["/usr/local/bin/heyu","onstate",codi]).rstrip()
    print("estrella marca {0}".format(estat.decode('ascii')))
    if estat.decode('ascii') == "1":
        print(codi+" encara estava engegat, parem")
        call(["/usr/local/bin/heyu","off",codi])

#Main
ENGINE_LOCK = "/usr/local/var/lock/LCK..heyu.engine.ttyUSB0"
if os.path.exists(ENGINE_LOCK)==False: 
    print("L'engine no estava en marxa, engeguem..")
    try:
        sortida = call(["/usr/local/bin/heyu","engine"])
    except Exception as e:
        print("No s'ha pogut engegar l'engine: {0}".format(e))

start = datetime.time(17, 50, 0)
end = datetime.time(22, 30, 0)
#end = datetime.time(18, 30, 0)
inrange = time_in_range(start, end, datetime.datetime.now().time())

if inrange:
    engega("A9")
    engega("A8")
else:
    apaga("A9")
    apaga("A8")
