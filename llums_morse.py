#!/usr/bin/env python3
import RPi.GPIO as GPIO
import getch
import time


CODE = {' ': ' ',
        "'": '.----.',
        '(': '-.--.-',
        ')': '-.--.-',
        ',': '--..--',
        '-': '-....-',
        '.': '.-.-.-',
        '/': '-..-.',
        '0': '-----',
        '1': '.----',
        '2': '..---',
        '3': '...--',
        '4': '....-',
        '5': '.....',
        '6': '-....',
        '7': '--...',
        '8': '---..',
        '9': '----.',
        ':': '---...',
        ';': '-.-.-.',
        '?': '..--..',
        'A': '.-',
        'B': '-...',
        'C': '-.-.',
        'D': '-..',
        'E': '.',
        'F': '..-.',
        'G': '--.',
        'H': '....',
        'I': '..',
        'J': '.---',
        'K': '-.-',
        'L': '.-..',
        'M': '--',
        'N': '-.',
        'O': '---',
        'P': '.--.',
        'Q': '--.-',
        'R': '.-.',
        'S': '...',
        'T': '-',
        'U': '..-',
        'V': '...-',
        'W': '.--',
        'X': '-..-',
        'Y': '-.--',
        'Z': '--..',
        '_': '..--.-'}
ledPin=17
GPIO.setmode(GPIO.BCM)
GPIO.setup(ledPin,GPIO.OUT)


def dot():
    GPIO.output(ledPin,1)
    time.sleep(0.2)
    GPIO.output(ledPin,0)
    time.sleep(0.2)

def dash():
    GPIO.output(ledPin,1)
    time.sleep(0.5)
    GPIO.output(ledPin,0)
    time.sleep(0.2)

def giraun():
    if GPIO.input(ledPin):
        GPIO.output(ledPin,GPIO.LOW)
    else:
        GPIO.output(ledPin,GPIO.HIGH)

def blink():
    try:
        while True:
            GPIO.output(ledPin,GPIO.LOW)
            time.sleep(0.5)
            GPIO.output(ledPin,GPIO.HIGH)
            time.sleep(1)
    except KeyboardInterrupt:
        pass

try:
    print('Escriu les lletres...')
    while True:
        letter = getch.getch()
        if letter == '1':
            giraun()
        elif letter == '2':
            letter=blink()
        else:
            for symbol in CODE[letter.upper()]:
                if symbol == '-':
                    dash()
                elif symbol == '.':
                    dot()
                else:
                    time.sleep(0.5)
            time.sleep(0.5)
except KeyError:
    GPIO.cleanup()
except KeyboardInterrupt:
    GPIO.cleanup()
